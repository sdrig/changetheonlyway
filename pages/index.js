import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Change the Only Way</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to change side :) 
        </h1>

        <p className={styles.description}>
          We are going to talk about why we should not afraid change things in our life.{' '}
        </p>

        <div className={styles.grid}>
          <a href="#" className={styles.card}>
            <h3>Believe in change! &rarr;</h3>
            <p>Without change what keeps internal motivation alive. Why we are dreaming about tomorrow if there will be no change at all!</p>
          </a>

          <a href="#" className={styles.card}>
            <h3>Open to change &rarr;</h3>
            <p>We even don't know what drives our life in our way. How would you say no if a change knocking your door?</p>
          </a>

          <a
            href="#"
            className={styles.card}
          >
            <h3>Ownership &rarr;</h3>
            <p>Ask yourself who is driving your life? Are you able take all responsibilities or blame somebody else because of where you are?</p>
          </a>

          <a
            href="https://vercel.com/import?filter=next.js&utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
            className={styles.card}
          >
            <h3>Team work &rarr;</h3>
            <p>
              You can change yourself toward which direction you want but team work and change with team work will be always harder.
            </p>
          </a>
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}
